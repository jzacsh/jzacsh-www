# jzacsh's personal website sourc ecode

Code for Jonathan Zacsh's personal website: https://j.zac.sh

## License

While the source code in this repository is licensed per the `LICENSE` file,
please note that the artwork (eg: the drawings) in `./static/img/artwork/...` is
all copyright Jonathan Zacsh. Contact me to talk about using it - I'm happy to
chat!

## Content Management

Blog/text content lives in the usual places hugo dicates.

Image (artwork) content: I _mostly_ go for a PESOS model, but also am _very
slowly_ continuing to develop my own in-codebase management of artwork I want
hosted here with its own content/tags/etc. (See "POSSE" below".

Here "PESOS" is an acronym "Publish Elsewhere, Syndicate (to your) Own Site"
described at https://indieweb.org/PESOS

### POSSE: Locally-Managed Contents

In a more [POSSE](https://indieweb.org/POSSE) model I'm slowly (years)
developing a in-site management of my artwork, and using the older "archival"
artwork as a way to test that workflow. One day I'll write more full-featured
navigation for this and other things, but in the meantime it's left in
"archival" mode on my drawings page. See TODOs section below for more on what's
blocking this development progress.

It's this effort, the artwork's metadata and thumbnail management, that accounts
for most of the custom logic in this codebase.

#### Artwork IDs

Internally artwork IDs [_should_ be stable][artworkIdLogic] and robust against
simple renames. Just be sure to rename in a way git follows clearly when you
check `git status` (eg: `git mv a.jpg b.jpg` without any further mods to `b.jpg`
within the same commit).

[artworkIdLogic]: https://gitlab.com/jzacsh/jzacsh.gitlab.io/-/blob/f095d6db053dc7264a6dbd67013f4e309d102cf8/src/bin/art-index-build.ts#L167

From there the usual logic that runs with build steps (see "Development" below)
will re-generate a new thumb and throw an error pointing out the old thumb that
needs to be deleted.

### PESOS from Instagram

Here are instructions on how to import my Instagram posts to my own blog. An
example of the final output of these steps can be seen in commit
`2455b7ad6e770f9c47d5ee01ec8fd7e8fbc29004`:

1. generate a zip file in JSON mode of your instagram account:
   https://www.instagram.com/download/request

2. inflate said zip of your instagram export; eg: let's say it inflates to this
   directory: `/absolute/path/to/my/igram-dump-inflated/`

3. generate an intermediate JSON (of the type
   `./src/bin/pesos/insta/posts.ts`) by running:

    ```sh
    $ ./src/bin/pesos/insta/munge-insta-json.ts \
      /absolute/path/to/my/igram-dump-inflated/content/posts_1.json \
      > /tmp/abs/path/intermediate.json
    ```

4. manually edit `/tmp/intermediate.json` to give each post a `title`
   field that's human-readable~ish prose

5. generate markdown files with your newly complete `intermediate.json`:

    ```sh
    $ ./src/bin/pesos/insta/posts-to-markdown.ts \
          /tmp/abs/path/intermediate.json \
          /absolute/path/to/my/igram-dump-inflated \
          ./content/sketchbook/
    ```

## Development

**tl;dr** _all_ human-written git commits should happen on branch `main` (once
you're happy with the local build via `make` local-testing) and `make deploy`
should handle the rest of the commits/deploys/push details.

**Quickstart**: just `make watch` in one terminal and `make devserver` in
another, then edit away. When everything's committed and working, then deploy
from main via `make deploy`.

This is a a repository of mostly text _(written in asciidoc)_ - and a tiny bit
of source code - that is automatically turned into a directory of static HTML
assets using:

  1. `hugo` binary to compile and manage content

     - `hugo v0.135.0+extended linux/amd64 BuildDate=unknown VendorInfo=nixpkgs`
     - `go version go1.23.2 linux/amd64`
     - deno & typescript:
       ```
       deno 2.0.2 (stable, release, x86_64-unknown-linux-gnu)
       v8 12.9.202.13-rusty
       typescript 5.6.2
      ```
  1. everything out-of-hugo via Make; eg:

     - simple Make commands: `build` and `deploy`
     - `src/` containing _all_ of the things make recipes depend on
        - _except_ typescript that unfortunately gets compiled
          directly in static directories, so therefore gets served out of them.
          This was a good-enough solution [at the time I figured it
          out](https://gitlab.com/jzacsh/glue#hugo-ts), but I'm happy to have
          this fixed so the TS lives somewhere else.

### TODOs

1. [ ] write e2e integration (UI-interaction) tests
1. [ ] finish web gallery at /drawings by giving each piece a unique URL somehow
1. [ ] fix utf-8 rendering bug; for some reason utf-8 emoji either got decoded
   from instagram's json incorrectly or it's being encoded into markdown
   incorrectly (see commit `2455b7a`) or hugo is somehow not transferring it
   correctly to static/ dir....? Anyway for a test case, look for the utf8 emoji
   for hands-clasped ("thanks") in
   `content/sketchbook/Gorillas-in-January_made-20230113/index.adoc`.

  - [x] ruled out that my use of `import` keyword with Deno (to parse JSON for
      me) is causing an issue
  - [x] ruled out that my vim settings are somehow causing an issue (when I go
      to edit the JSON by hand in later steps). Proved this by writing the munge
      step directly to an html file (with some key markdown in `<head>` already
      set) and just pending (`echo`ing) a closijng `</html>` onto the file;
      still the file shows broken decoding by my browser.
      - **however** this _does_ prove that it's some early part of my pipeline
        that's broken. It's literally some step _before_  (or including) my
        munging of the JSON where the breakage happens.
  - [x] from previous bullet, **narrowed down issue** to just the munging stage;
      repeating the last bullet but with instagram's own `_posts.json` itself
      (so no script of my runs) I see HTML that renders with `\u` escape
      sequences (eg: `Pilot Kak\u00c3\u00bcno`) which is better than the
      post-munge state of `Pilot KakÃ¼no`.

### Git Branches

  1. Source & content: `main` branch
  2. generated website: `www` branch

### Commands:

Build static content of this repo into HTTP static files for serving:
```bash
make build
```

#### Advanced

To make changes and live reload in your browser:
```bash
hugo server
```

To see final (CDN-ready) contents of `make build`:
```bash
xdg-open tmp/  # inspect

python3 -m http.server  # and serve it, if you'd like
```

#### Setup

Just one-time, after *first* installing Hugo somehow; eg: if you don't like your
package manager's version then:

```bash
go get github.com/spf13/hugo
```

### Deploying

**tl;dr** hosting is _currently_ done by gitlab sites CDN. Commands below in
this section and then _other_ CDNs instructions further down for posterity.

History of deploys can be seen on the `www` branch
[history in gitlab's "graph" pane](https://gitlab.com/jzacsh/jzacsh.gitlab.io/-/network/www)


To automatically generate _(`make buildall`)_ and deploy _(`git {commit,push}`)_
a new `www` branch to gitlab:
```bash
make deploy
# if adapting, then read the variables passed inside this make target's script,
# first!
```

#### Deploying to AWS

A proven method that worked for me _previously_ - when Github pages didn't allow
for HTTPS static sites - was hosting on AWS Cloud Front. At some point the following worked:
```bash
make S3_BUCKET=my-bucket-name deployAws
```

And before AWS, I was using KeyCDN successfully as well (wiih `make
KEYCDN_ZONE=myzone deploy`).

#### Deploying to Google Cloud

I've learned that one can _also_ follow [this Google Cloud Storage
guide][googStaticSite] to serve most of this content via Google Cloud, but then
you'll find [the limitation that you cannot use HTTPs][googNoHttps].

[googStaticSite]: https://cloud.google.com/solutions/web-serving-overview#static-site
[googNoHttps]: https://cloud.google.com/storage/docs/troubleshooting#https

```bash
make GCS_BUCKET=my-bucket-name deployGcs
```
