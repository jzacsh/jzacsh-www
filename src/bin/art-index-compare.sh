#!/usr/bin/env bash

declare -r json_file="$1"
[[ -n "$json_file" && -f "$json_file" ]] || {
  printf 'JSON file argument expected (got "%s")\n' "$json_file" >&2
  exit 1
}

function catJsonHead() ( set -x; git show HEAD:"$json_file"; )

# returns 1 if there is stuff uncommitted, or untracked in the repo
# TODO sync/replace this with yabashlib
isRepoDirty() {
  test -n "$(git diff --shortstat 2>&1)" ||
    test -n "$(git status --porcelain 2>&1)"
}

function json() { python -mjson.tool; }
function jsonBefore() { catJsonHead | json; }
function jsonAfter() { json < "$json_file"; }

function isJsonChanged() {
  isRepoDirty &&
      ! diff <(jsonBefore) <(jsonAfter) >/dev/null 2>&1
}

# Nothing to compare, exit silently as if we weren't called
isJsonChanged || exit 0

# art index is dirty; show what really changed in our index
printf \
  'WARNING: art index is dirty! here is what changed on %s\n' \
  "$json_file" >&2

# TODO fiure out how to utilize $DIFF env variable
diff --color=auto -u \
  <(jsonBefore) \
  <(jsonAfter)
