import {
  FilePath,
  assert,
} from './common.ts';

export type InstagramTimestamp = number;

export function instagramTimestampToDate(stamp: InstagramTimestamp): Date {
  assert(stamp > 0, `got non-positif stamp: "${stamp}"`)
  return new Date(stamp * 1000);
}

export interface InstagramPostMedia {
  // filepath to the piece of media, relative to the root of the instagram zip
  uri: FilePath;
  creation_timestamp: InstagramTimestamp;
  media_metadata: {
    photo_metadata: unknown;
  };
  title: string;
  cross_post_source: unknown;
}

export interface InstagramPostMultiMedia {
  title: string,
  creation_timestamp: InstagramTimestamp;
  media: Array<InstagramPostMedia>
}

export type InstagramPosts = Array<InstagramPostMedia|InstagramPostMultiMedia>;
