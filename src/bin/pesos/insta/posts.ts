import {
  mustParseJsonFromPath,
  FilePath,
} from './common.ts';

export interface PostsListing {
  posts: Array<OriginalPost>;
}

/* more useful munge of {@link InstagramPosts}. */
export interface OriginalPost {
  title?: string
  media: Array<FilePath>;
  prose: string;
  created: Date;
}

export async function mustParsePostsFromPath(filePath: FilePath): PostsListing {
  const listing = await mustParseJsonFromPath(filePath);
  const posts = listing.posts.map(post => {
    post.created = new Date(post.created);
    return post;
  });
  return {posts}
}
