export type FilePath = string;

const { readTextFile } = Deno;

// This is to fiddle with the utf8 bug I'm seeing.
// So far as I can tell, enabling this changes nothing in the byte-difference
// betwewen two runs of munge-insta-json.ts
const experimentWithManualParse = true;

export function assert<T>(assertion: T, msg: string): T {
  if (assertion) return assertion;
  throw new Error(`assertion failed: ${msg}`);
}

export function dieError(msg: string) {
  console.error(msg);
  Deno.exit(1);
}

export function userAssert<T>(assertion: T, msg: string): T {
  try {
    return assert(assertion, msg);
  } catch (e) {
    if (assertion) return assertion;
    dieError(`error: ${msg}\n`);
  }
}

export async function mustParseJsonFromPath(filePath: FilePath): any {
  if (experimentWithManualParse) {
    return JSON.parse(await readTextFile(filePath));
  } else {
    try {
      return (await import(filePath, {
        assert: { type: "json" },
      })).default;
    } catch (e) {
      dieError(`failed to parse JSON from: ${filePath}: ${e}\n`);
    }
  }
}
