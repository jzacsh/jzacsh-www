#!/usr/bin/env -S deno run --allow-write --allow-env --allow-read --allow-run

import {
  ArtworkMetadata,
  ArtworkMetadataType,
  isValidArtworkMetadataKey,
  listValidArtworkMetadataTypes,
  ArtworkDir,
  ArtworkMetadataFreq,
  ArtMetadataHash,
  ArtworkMetadataStats,
  ArtworkMetadataVal,
  ArtworkMetadataValues,
  Artwork,
  ArtworkIndex,
  ArtworkDirChild,
  sortArtworkDirChild,
  buildDefaultValueFreq,
} from './ts/artapi.ts';

import {
  StaticHost,
  TargetSmallWidthPx,
} from './ts/art.ts';

import {assert} from './ts/assert.ts';

import {
  keepNDecimals,
  digestOf,
} from './ts/common.ts';

import {
  Opt,
  assertOpt,
  optIsEmpty,
  optEmpty,
} from './ts/optional.ts';

import {
  fsPathJoin,
  FsFilePath,
} from './ts/fs.ts';

import {
  gitFindOrigin,
  gitIsDependentFresh,
  OPTS as GitOpts,
  GitOrigin,
} from './git.ts';

import {
  Geometry2D,
  NotAnImageError,
  getImageSize,
  isImageWidth,
} from './image.ts';

import {
  resizeImageWidth,
} from './resize.ts';

import {
  fileExists,
  readTextFileIfExists,
} from './file.ts'

/**
 * Generates a mildly useful JSON tree of the image that should be available as
 * as assets to reference (eg: from <img /> tags) without any issue.
 *
 * NOTE: this was documented as working as of:
 *     ```sh
 *     $ deno --version
 *     deno 1.29.1 (release, x86_64-unknown-linux-gnu)
 *     v8 10.9.194.5
 *     typescript 4.9.4
 *     ```
 */

interface ProgOpts {
  staticHost: StaticHost;

  // Whether we'll only lightly sample directories, and print lots of annoying
  // logs.
  isDebugging: boolean;
}

const OPTS = {
  DbgModeMaxChildren: 20,
  MinMetadataValueThreshold: 5,
  MetadataFileSuffix: 'metadata',
  DecimalPrecision: 3,
  ExpectedImageSuffixes: [
    'jpg', 'jpeg', 'png', 'webp',
    'gif', 'svg', 'tif',
  ],
};

function buildProgOptions(args: Array<string>): ProgOpts {
  const usageHelpStr = 'usage: [-debug[:DisableParseGitDiffNameOnly]] HUGO_DIR';
  assert(args.length, `expected a directory of art: ${usageHelpStr}`);
  let isDebugging = false;
  let filepathArgIndex = 0;
  if (args.length == 2) {
    const firstArg = args[0] || ''; // impossible; just for typescript compiler
    assert(firstArg.startsWith('-debug'), usageHelpStr);
    filepathArgIndex = 1;
    isDebugging = true;
    GitOpts.ParseGitDiffNameOnly = !firstArg.includes('DisableParseGitDiffNameOnly');
  }
  let filepathArg = (args[filepathArgIndex] || '').trim();

  GitOpts.isDebugging = isDebugging;
  return {
    staticHost: new StaticHost(filepathArg),
    isDebugging,
  };
}

const FLAGS = buildProgOptions(Deno.args);

///////////////////////////////////////////////////////////
// basic, business-agnostc lib ////////////////////////////

const stderr = console.warn;
function _logPrefix(
  logger: Function,
  prefix: string,
  msg: string,
  ...opts: unknown[]
) {
  const format = `${prefix}: ${msg}`;
  logger(format, ...opts);
}

// specifically use stderr because stdout should only be used for JSON data itself
const info = (msg: string, ...args: unknown[]) => _logPrefix(stderr, 'STATUS', msg, args);
const warn = (msg: string, ...args: unknown[]) => _logPrefix(stderr, 'WARNING', msg, args);
const dbg = (msg: string, ...args: unknown[]) => {
  if (!FLAGS.isDebugging) return;
  _logPrefix(stderr, 'DBG', msg, args);
}

// only useful for debugging
/*
async function softTry<T>(fn, fallback: T): T {
  try {
    return await fn();
  } catch (e: unknown) {
    console.error(`MASKING FAILURE!! got\n"""\n${e}\n"""\n`);
    return fallback;
  }
} // */

// end of basic, business-agnostic lib ////////////////////
///////////////////////////////////////////////////////////

function prepFloat(num: number): number {
  return keepNDecimals(num, OPTS.DecimalPrecision);
}

/**
 * Computes a hash for `artworkFilePath` that is unlikely to break by accident.
 */
async function computeStableId(artworkFilePath: string): Promise<string> {
  const gitOrigin: GitOrigin = await gitFindOrigin(artworkFilePath);
  // Include *both* original commit and filepath because original commit itself
  // could have other files in it as well, so that alone could cause such an ID
  // to collide with other files that would use this same computation.
  const uniqueIdOfFile: string = [
    gitOrigin.originatingCommitId,
    gitOrigin.originalFilepath,
  ].join('|');

  const digestOfUniqueId: string = await digestOf(uniqueIdOfFile);
  return digestOfUniqueId;
}

function buildMetadataPath(filepath: FsFilePath): FsFilePath {
  return `${filepath}.${OPTS.MetadataFileSuffix}`;
}

function startNewArtdir(filename: string): ArtworkDir {
  return {
    filename,
    children: [],
    immediateLength: 0,
    recursiveLength: 0,
  }
}

function addChildToArtdir(artDir: ArtworkDir, subDir: ArtworkDirChild) {
  artDir.children.push(subDir);

  // TODO this calc. is flawed, so if I ever care about this, rewrite this;
  // (test case: put a file in a deeply nested directory, the parents of which
  // have no contents of their own).
  artDir.recursiveLength = 0;
}

function calculateRatio(width: number, height: number): number {
  if (height <= 0 || !width) return 0; // avoid division by zero
  const rawRatio = width / height;

  return prepFloat(rawRatio);
}

function buildArtwork(
  filename: string,
  artworkId: string,
  width: number,
  height: number,
  metadata: ArtworkMetadata,
): Artwork {
  return {
    filename,
    artworkId,
    ratio: calculateRatio(width, height),
    width,
    height,
    metadata,
  };
}

interface Dir {
  rootPath: string;
  entry: Deno.DirEntry;
}

class Queue {
  rootDir: string;
  private q: Array<Dir>;

  constructor(rootPath: string) {
    assert(
      (rootPath || '').trim().length,
      'rootPath cannot be empty');
    this.rootDir = rootPath;
    this.q = [];
  }

  addDir(dir: Dir) {
    this.q.push(dir);
  }

  next(): Dir {
    assert(!this.isEmpty(), 'called next() on empty queue');
    return assert(
      this.q.shift(),
      'bug: not isEmpty(), yet shift() had no content');
  }

  isEmpty() {
    return !this.size();
  }

  size() {
    return this.q.length;
  }
}

/**
 * Hacky heuristic for whether an extant thumbFilePath exists that already
 * represents `origFilePath` sufficiently (at a thumbnail width of
 * `targetThumbWidth` px).
 */
async function isThumbnailStale(
  origFilePath: FsFilePath,
  thumbFilePath: FsFilePath,
  targetThumbWidth: number,
): Promise<boolean> {
  const isThumbGeoFresh = async(filePath: FsFilePath, targetWidth: number): Promise<boolean> => {
    try {
      return await isImageWidth(filePath, targetWidth);
    } catch (e) {
      const exists = await fileExists(filePath);
      if (!exists) {
        console.error(`assuming trivial staleness: thumb was never made yet? ${filePath}`);
        return false;
      }
      throw e; // not an expected error, so continue crashing.
    }
  };

  const isGitFresh = await gitIsDependentFresh(origFilePath, thumbFilePath);
  const isGeoFresh = await isThumbGeoFresh(thumbFilePath, targetThumbWidth);
  return !isGitFresh || !isGeoFresh;
}

async function loadMetadataForArtwork(artworkFilePath: string): Promise<Opt<ArtworkMetadata>> {
  const metadataFilePath = buildMetadataPath(artworkFilePath);
  let maybeContents = await readTextFileIfExists(metadataFilePath);
  if (optIsEmpty(maybeContents)) {
    return optEmpty<ArtworkMetadata>();
  }
  const contents: string = assert(
    maybeContents,
    'bug? somehow undefined let through our early return');
  assert(String(contents).trim(), `found effectively-empty metadata file: ${metadataFilePath}`);

  type Line = {
    line: string,
    index: number,
  };

  type KeyVal = {
    key: keyof ArtworkMetadata,
    value: string,
  };

  type KeyValSourceCode = Line & KeyVal;

  const lines: Array<Line> = contents.
      split('\n').

      // Encode original file's line numbers, for future error messages
      map((line, index) => {
        return {line, index};
      });

  function parserMsg(lineNumber: number, errorMessage: string) {
    const ln = lines[lineNumber];
    const content = ln ? ln.line : `[build bug: missing line for this lineNumber(${lineNumber})]`;
    return `metadata parsing error on line ${
        lineNumber} of "${
        metadataFilePath}": ${
        errorMessage}; line:\n"${
        content}"`;
  }

  function parseLineToKeyVal(ln: Line): KeyVal {
    const keyValDelim = ':';
    let parts: Array<string> = ln.line.split(keyValDelim);
    assert(
        parts.length > 1,
        () => parserMsg(ln.index, `missing a key-value delimeter('${keyValDelim}')`));
    if (parts.length !== 2) {
      warn('consuming blindly: : '+ parserMsg(ln.index, `too many key-value delimeters('${keyValDelim}')`));
      const firstPart = assert(parts[0], 'validated index access');
      parts = [
        firstPart,
        ln.line.substr(firstPart.length + 1 /*include the delim itself*/),
      ];
    }
    const [keyRaw, valRaw] = parts.map(part => part.trim());
    const keyUnchecked: string = assert(keyRaw, () => parserMsg(ln.index, `empty key (line="${ln.line}")`));
    const val: string = assert(valRaw, () => parserMsg(ln.index, `empty value (line="${ln.line}")`));

    assert(
      isValidArtworkMetadataKey(keyUnchecked),
      () => parserMsg(ln.index, `unregistered key ("${keyUnchecked}"); typo? if not, just add it to ArtworkMetadata definition`));
    let key = keyUnchecked as keyof ArtworkMetadata; // per isValidArtworkMetadataKey assertion above

    return {key, value: val};
  }

  return lines.

      // Remove empty and comment lines
      filter(ln => ln.line && ln.line.trimStart()[0] !== '#' && ln.line.trim()).

      // Parse keys and values
      map((ln: Line): KeyValSourceCode => {
        return {
          ...ln,
          ...parseLineToKeyVal(ln),
        }
      }).

      // Collect keys and values into ArtworkMetadata object
      reduce((metadata, ln: KeyValSourceCode) => {
        metadata[ln.key] = metadata[ln.key] || [];
        assert(metadata[ln.key], 'bug: just ensured entry').push(ln.value);
        return metadata;
      }, {} as ArtworkMetadata /*initialValue*/);
}

async function processDirEntry(pathToList: string, q: Queue, entry?: Deno.DirEntry): Promise<ArtworkDir> {
  assert(!entry || entry.isDirectory, 'processDirEntry called on a non-dir: ${dir.name}');

  dbg(`pathToList="${pathToList}"`);
  const artDir: ArtworkDir = startNewArtdir(pathToList);
  let directChildCount = 0;
  for await (const dirEntry of Deno.readDir(pathToList)) {
    if (FLAGS.isDebugging && directChildCount > OPTS.DbgModeMaxChildren) break;
    directChildCount++;

    let artworkFilePath: string = fsPathJoin(pathToList, dirEntry.name);
    if (dirEntry.isDirectory) {
      dbg(`under "${pathToList}" got DIR: "${dirEntry.name}" (q size ${q.size()})`);
      q.addDir({rootPath: artworkFilePath, entry: dirEntry});
      continue;
    }

    if (artworkFilePath.endsWith(OPTS.MetadataFileSuffix)) continue;

    let geometry: Geometry2D|NotAnImageError = await getImageSize(
        artworkFilePath,
        OPTS.ExpectedImageSuffixes);
    if (geometry instanceof NotAnImageError) {
      warn(`skipping over non-art file found: "${artworkFilePath}"`);
      continue;
    }

    dbg(`under "${pathToList}" got FILE: "${dirEntry.name}" (q size ${q.size()})`);
    // TODO consider Promise.all() for both these filesystem isnpections, so we
    // can get _slightly_ faster build times
    const artworkId: string = await computeStableId(artworkFilePath);

    artDir.immediateLength++;
    const artworkMetadata: ArtworkMetadata = assertOpt(
      await loadMetadataForArtwork(artworkFilePath),
      `failed loading metadata for: ${artworkFilePath}`,
    );
    cacheStats(artworkMetadata);

    const artwork: Artwork = buildArtwork(
      artworkFilePath,
      artworkId,
      geometry.width,
      geometry.height,
      artworkMetadata);

    const targetThumbFilepath = FLAGS.staticHost.toThumbPath(artworkFilePath);
    const targetWidthPx = TargetSmallWidthPx * 2;
    const isStaleThumb = await isThumbnailStale(artworkFilePath, targetThumbFilepath, targetWidthPx);
    if (isStaleThumb) {
      console.error(`resizing apparently stale thumbnail to ${
        targetWidthPx}! from ${geometry.width} ${
        artworkFilePath}\n\t${
        targetThumbFilepath}\n`);
      await resizeImageWidth(
          {filepath: artworkFilePath, geo: geometry},
          targetThumbFilepath,
          targetWidthPx);
    }

    addChildToArtdir(artDir, artwork);
  }
  // TODO (idempotent JSON generation): everytime a dir is walked, sort its
  // children, by something very stable, eg: artworkId. Right here (after
  // looping over immediate children), is a perfect time to do it.
  artDir.children.sort((a, b) => sortArtworkDirChild(a, b));

  // initial baseline; this number will be ammended by further walks
  artDir.recursiveLength = artDir.immediateLength;

  while (!q.isEmpty()) {
    const nextDirToWalk = q.next();
    const subPath = nextDirToWalk.rootPath;
    dbg(`walking, "${subPath}", ${q.size()}`);
    const subArtworkDir: ArtworkDir = await processDirEntry(subPath, new Queue(subPath));
    if (subArtworkDir.children.length === 0) {
      continue;
    }
    addChildToArtdir(artDir, subArtworkDir);
    artDir.recursiveLength += subArtworkDir.recursiveLength;
  }
  artDir.children.sort((a, b) => sortArtworkDirChild(a, b));
  return artDir;
}

// TODO delete this type; just trying to unwrangle tsc complaints...
type MapArtFreq = Map<ArtworkMetadataVal, ArtworkMetadataFreq>;
// TODO delete this type; just trying to unwrangle tsc complaints...
type MapMapArtFreq = Map<keyof ArtworkMetadata, MapArtFreq>;

function metadataValueReducer(stats: MapArtFreq, val: ArtworkMetadataVal): MapArtFreq {
  let stat: ArtworkMetadataFreq =
    assert(stats.get(val), `bug? Map#has("${val}") was missing`);
  stat.count++;
  stats.set(val, stat);
  return stats;
}

interface ArtFreqCompute {
  artFreq: MapMapArtFreq;

  // pre-computed store of empty ArtworkMetadataFreq objects
  valStore: MapArtFreq;
};

function forEachPresentValList(
  artworkMetadata: ArtworkMetadata,
  func: (key: ArtworkMetadataType, vals: ArtworkMetadataValues) => void,
) {
  interface KeyVals {
    key: ArtworkMetadataType;
    vals: ArtworkMetadataValues;
  };
  listValidArtworkMetadataTypes().
    map((key, idx, arr): KeyVals => {
      return {key, vals: artworkMetadata[key]};
    }).
    filter((keyVals: KeyVals, idx, arr) => {
      return !!keyVals.vals && !!keyVals.vals.length;
    }).
    forEach((keyVals: KeyVals, idx, arr) => {
      func(keyVals.key, keyVals.vals);
    });
}

/**
 * Reducer that's run once over each artwork's metadta, and extracts its inner
 * metadata values' frequencies out to a larger reduction (starting a new
 * frequency if the artwork's the first to introduce a particular
 * ArtworkMetadataVal)..
 */
function collectStatsFromSingleArt(reduction: ArtFreqCompute, metadata: ArtworkMetadata): ArtFreqCompute {
  forEachPresentValList(metadata, (key, presentVals: ArtworkMetadataValues) => {
    const extantStats: MapArtFreq =
        reduction.artFreq.has(key) ?
        assert(reduction.artFreq.get(key), `bug? Map#has("${key}") failed us for reduction`) :
        reduction.valStore;
    const augmentedFreqs = presentVals.reduce(
      (red, item) => metadataValueReducer(red, item),
      extantStats /*initialValue*/,
    );
    reduction.artFreq.set(key, augmentedFreqs);
  });
  return reduction;
}

async function computeHashes(m: Array<ArtworkMetadata>): Promise<Map<ArtworkMetadataVal, ArtworkMetadataFreq>> {
  const valsPresent: Set<ArtworkMetadataVal> = new Set();
  m.forEach(metadata => {
    forEachPresentValList(metadata, (k, presentVals: ArtworkMetadataValues) => {
      presentVals.forEach(v => valsPresent.add(v));
    });
  });
  const hashes: Map<ArtworkMetadataVal, ArtworkMetadataFreq> = new Map();
  for (let val of valsPresent) {
    const hash = await buildDefaultValueFreq(val);
    hashes.set(val, hash);
  }
  return hashes;
}

async function computeMetadataStats(
  artworksMetadata: Array<ArtworkMetadata>,
  artworkCount: number,
): Promise<ArtworkMetadataStats> {
  assert(
      artworksMetadata.length <= artworkCount,
      `expected no more metadata (got ${artworksMetadata.length} than known works of art (${artworkCount})`);

  // TODO add special-casing for the "created" metadata so meaningful filtering
  // can happen (eg: by year, by month

  const valStore: Map<ArtworkMetadataVal, ArtworkMetadataFreq> = await computeHashes(artworksMetadata);
  const artFreq: MapMapArtFreq = new Map<keyof ArtworkMetadata, MapArtFreq>();
  const rawStats: ArtFreqCompute = artworksMetadata.reduce(
    (reduction, item) => collectStatsFromSingleArt(reduction, item),
    { valStore, artFreq } /*initialValue*/,
  );

  return Array.
      from(rawStats.artFreq.entries()).
      reduce((retainedStats, item) => {
        const [metadataKey, rawMapOfValues] = item;
        let frequentedValues: Array<ArtworkMetadataFreq> = Array.
            from(rawMapOfValues.values()).
            filter(stat => stat.count >= OPTS.MinMetadataValueThreshold).
            map(stat => {
              stat.freq = prepFloat(stat.count / artworkCount);
              return stat;
            });

        retainedStats[metadataKey] = frequentedValues;
        return retainedStats;
      }, {} as ArtworkMetadataStats /*initialValue*/);
}

// end of business logic libs /////////////////////////////
///////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////
// Start of main executions ///////////////////////////////

const cachedStats: Array<ArtworkMetadata> = [];
function cacheStats(metadata: ArtworkMetadata) {
  cachedStats.push(metadata);
};

const q = new Queue(FLAGS.staticHost.sketchbookDir);
info(`starting crawl of art dir: ${FLAGS.staticHost.sketchbookDir}`);
// TODO delete all old thumbnails before index building starts
const contents = await processDirEntry(q.rootDir, q);
info(`done with crawl of art dir; computing stats`);
// TODO factor out computeMetadataStats to a separate module, along with all its
// depenent local functions.
const metadataStats = await computeMetadataStats(cachedStats, contents.recursiveLength);
const index: ArtworkIndex = { contents, metadataStats };
info(`done building index; printing`);

await Deno.writeTextFile(
  FLAGS.staticHost.getIndexPath(),
  JSON.stringify(index));
