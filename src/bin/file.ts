import {
  optEmpty,
  Opt,
} from './ts/optional.ts';

import {
  FsFilePath,
} from './ts/fs.ts';

/** Returns text content of a `filePath` if it exists, otherwise returns null. */
export async function readTextFileIfExists(filePath: FsFilePath): Promise<Opt<string>> {
  try {
    return await Deno.readTextFile(filePath);
  } catch (e) {
    if (e instanceof Deno.errors.NotFound) {
      return optEmpty<string>();
    }
    throw e;
  }
}

export async function fileExists(filePath: FsFilePath): Promise<boolean> {
  try {
    await Deno.lstat(filePath);
  } catch (err) {
    if (err instanceof Deno.errors.NotFound) {
      return false;
    }
    throw err; // unrelated error; bubble
  }
  return true; // no error
}
