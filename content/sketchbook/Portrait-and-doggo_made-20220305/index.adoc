+++
title = "Portrait and doggo - made 20220305"
type = "post"
date = "2023-04-13"
publishdate = "2023-04-13"
+++

Another portrait, and another doggo, all in one sketchbook page from last month. No idea what I was doing with that figure on the left side. Pleased with the portrait though - looks reasonably close to the subject.

Drawn 2022-03-05 in pen & ink (no idea which pen), about A5 sized paper `#inkdrawing` `#fountainpen` `#livemodel`

image::341108536_649987570298402_75989190878868256_n_18018287104541768.jpg[]
