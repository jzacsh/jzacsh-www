+++
title = "Drawing: A Visual Thinking Tool"
toc = true
date = 2024-07-07
type = "post"
+++

:20240707shownTell: https://instagram.com/reel/C9QZ_NwvLqQ
:talkPic: 20240707-talk-photo.webp
:talkPicDogToy: 20240707-talk-photo_dog-toy-model.webp
:toc:

What I think the process of drawing _fundamentally is_, in just five minutes. On
July 7th 2024 I gave a live, lightning-fast 5 min that included a live demo in
Chicago at {20240707shownTell}[Show n Tell for Grownups]. This write-up is based
on a months-later transcription of that talk, from memory.

I got enough positive feedback from this live session that I've since started
teaching this to small groups for fun as live one-hours sessions. It's been
quite a fun journey for me, so feedback welcome!

== Background: Drawing from Observation

I'm a software engineer by day, but I've been _drawing_ everyday for 18 months.

When people see my drawings or learn that I draw, often their first reaction is
a mix of compliments and self-deprecation. But it's the particular nature of
comparison that gets me: it's always...

* _"I wish **I could** draw"_,
* _"I wish I could draw **more easily**"_
* _"I wish I could draw **well**"_.

In the more extended, intimate conversations this always leaves me wishing I
could share why I think the joys of drawing can be **more accessible than folks
think**. (Why we all think so unconstructively about drawing _in the first
place_... well that's for another talk).

Offering perspective: the thing that **compels me** in drawing is that it is an
easy process that is fun and low stakes. If it wasn't I never would've gotten
off the ground drawing and I still wouldn't probably draw today. So I'm going to
try to convey a simple approach to thinking to sort of help you try out what I
do, that I think would work for anybody.

== Overview: Drawing is Mostly Lots of Thinking

[link={20240707shownTell},window=_blank]
image::{talkPic}[width=300px, role="inlineimg inlineimg-small inlineimg-right", title="Giving this talk for the first time 2024-07-07"]

First shift how you think about drawing--executing or having a particular
expectation of drawing--to being a reflection. So you're not producing a drawing
that is going to _be_ a certain way, but you're going to reflect on something
that you've observed, and the drawing process will drive that. So I'd liken it
to say: you wouldn't stop yourself from journaling in your personal diary or
notebook just because you're not Margaret Atwood. You _know_ you're allowed to
write--you don't have to be Neil Gaiman to think on paper. You wouldn't expect
to be a great orator to speak out loud in the shower or to yourself while
walking your dogs.

Drawing is just a tool for thinking, and your craft only improves as if you
learn to indulge in that thinking time.

=== Practice: Interactive Warning: Follow Along!

So I'm going to walk you through very concrete steps now: that "tool for
thinking." If you have paper and pen, follow along now: you can actually start
writing some things down.

.There's three physical tools and four step loop; the **three tools** are:
1. **a pen** (not a pencil if you can help it),
2. **paper** or notebook/sketchbook
3. **blank page** preserved/bookmarked at the front
.. We'll call this page your "**prompts list**" (details coming)
.. This is empty for now (but feel free to label it up top)
.. This page is really key to the process! This is where we'll apply our loop of
   4 steps. It's a space you'll cultivate personal findings.

[#highLevelLoop]
== High-Level Process: An Internal Conversation, on Loop

So those are the three tools, and they're really surrounding your prompts list.
Maybe even title that first blank page: _"**Prompts List**: trivial-question
generator"_ at the top--something to _remind yourself of the simple nature_ of
the process of drawing, and what all these little "prompts" are that you'll
collecton this page.

.You may find it helpful to **write these four simple steps** on the back of your prompts-page:
. Pick something to draw, something you're observing
. Literally actually draw
. Stop for some reason; examples:
.. Eg: ran out of time (maybe you're following a prompt that says "draw for 3
 minutes")
... Eg: you got bored
... Eg: you find the drawing is now illegible
. (critical too-few people IMO) each of those reasons to stop is a chance to
  inject a new prompt or write a reflection about what just happened. Something
  trivial: like a half-sentence, a couple words. Examples:
.. Eg: stopped because "ran out of time"
... Great! How did the timed-prompt you were following go? How do you feel about
    it? What can you try different now, based on that answer?
.. Eg: stopped because "got bored"
... Great! Golden info on what's interesting to you right now: take that to
    your prompts list or maybe it'll make for an obvious new prompt!
.. Eg: stopped because "became illegible"
... Best one, easiest to tackle: when in the drawing process did that
    happen? What about it felt more illegible than moments before? Write
    this all down beside/within the drawing. Use that as a spurious prompt
    to re-draw some of it!

So hopefully the sneaky trick here is clear: you're looping back on yourself,
starting with a "prompt" the goal is not to answer, but to experience the
drawing, and produce another prompt to ... do what? Experience more drawing! The
only difference: every time around the loop you've magically _taught yourself_
something small! A self-perpetuating machine for curiosity and visual-thinking.

=== Goal Oriented: Don't Obsess over this Process

While I really believe this process is helpful to me and wish I'd understood it
sooner, keep in mind that you'll know if you're productively growing as a
tactician. Ultimately I've modeled what I think is the tactical, simple way that
good artists cultivate their own skills with their three simple tools (pen,
paper, mental space for notes-to-self (literal "prompts list" in my case)).

**Goal**: try the examples with me, but keep in mind what your ultimate goal is:
to become cognizant of your drawing process; to give yourself explicit space to
reflect cultivate helpful patterns. **Example**: maybe you'll only use my
example prompts for a while, or only ones you find online. That's great! But
just make sure you're always _listening to yourself_ as your draw, and then
_reflecting_ in the pages of your sketchbook right beside your drawings. That
foreign-looking prompts list on page one will start growing organically the more
you've let yourself reflect.

[#examplePrompts]
=== Quick Example Prompts

Alright let me share with you a couple more prompts now: these are "real"
prompts now: they're trivial unlike that mega-prompt: just a few words, the
kinds of things you'll be generating all the time.

[#promptTrivialItem]
===== Prompt: Trivial Observable

Here's a favorite prompt of mine: write "trivial observable" as the title. Here
you'll find something in your environment that is so boring, so mundane--a
light switch, a coin--something you think no one would ever draw and you'd never
draw and wouldn't be worth your time. Draw that! It is a great prompt to follow.
It's going to--if you follow those four steps as you follow this prompt--it's
going to produce lots of wonderful reflection for you.

[#promptBlindContour]
==== Prompt: Blind Contour

Okay one more favorite prompt, I'll demo for you real fast in the last 30
seconds or so... write this one down as "ant walking," and get ready to try
as soon as I'm done explaining:

image::{talkPicDogToy}[, role="inlineimg inlineimg-small inlineimg-right", title="Close-up of my dog Hippo's favorite lion-toy I used as my model"]


. I'm going to draw that lion
. I'm going to imagine an ant standing on it
. Okay so I'm going to put my pen on the paper where the ant is
. Then I'm going to start drawing the ant as it walks along the edge, the outer
. Edge of the line as I see it.
. Now notice I'm _not_ looking at my drawing as I do it
.. The more popular name for this is "blind contour"; it's famous for a reason:
   produces lots of good reflection for you
.. So you can see here: i got a vague shape that sort of resembles an animal,
   but it's not a masterpiece, it's just a tool for reflection.

[#concludingRecap]
== Recap: Just Four Steps

So the key here is that step four: when you stop your process, you'll have some
insights, something that you write down that will sound so generic, so obvious
that you'll think _"that's so stupidly obvious, I'm not going to write it
down"_--that's the one to write down! Put that one back into your prompts list,
as there's clearly lots for you to unpack there, so it'll yield a lot the next
time you see it (obviously: especially if you leave yourself some
reminder example-notes beside it!).
