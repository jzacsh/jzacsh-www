+++
title = "Hippo napping - made 20230217"
type = "post"
date = "2023-04-11"
publishdate = "2023-04-11"
+++

Both these pages are from the same day - both of my dog napping. A favorite activity. These are sketches of my 8lb, floppy-eared terrier back in February. I might've been testing out different pens at the time too, hence the funny colors.

Favorite dog-napping position: the "snout under paw-bridge" move. I'm assuming other dog-people will recognize this.

Drawn 2023-02-17 in pen & ink (TWSBI vac mini w/stock nib, Pilot KakÃ¼no, and Picasso 916; no idea which inks), about A5 sized paper. `#fromlife` `#inkdrawing` `#fountainpen`

image::340304765_234102072477988_8313661560592930984_n_17869466831842327.jpg[]
image::340496069_143799341981625_8005316201792392109_n_17943148850618030.jpg[]
