+++
title = "Lions winter sun - made 20230115"
type = "post"
date = "2023-03-19"
publishdate = "2023-03-19"
+++

Couple more quick sketches I did at https://instagram.com/lincolnparkzoo[@lincolnparkzoo, window=_blank] - this time of the lions.

They're surprisingly tranquil, as they're always focused on soaking up the winter sun, hanging on their rock. This means they make for easy studies as I can fill up a page of sketches of the same position or pose quite easily as opposed to the chimpanzees that are way more fidgety (unless they're out for a nap on their backs).

Particularly fun aspect of studying them has been trying to capture what makes their mouth shape feel iconic of lions. I'll post more sketches of them soon, I think I capture their jowls a bit more in recent visits.

Drawn 2023-01-15, 2023-02-18, both pen and ink, about A5 sized paper. `#zoo` `#inkdrawing` `#fountainpen`

image::336807583_648763800392176_8232961390442310960_n_17987727766904531.jpg[]
image::336337225_886350669062788_4359247273352297423_n_17884416074810689.jpg[]
