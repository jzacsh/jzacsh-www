+++
title = "Figure 'Curriculum': Studying & Learning Educational Direction"
type = "page"
+++


This is a living, breathing "art degree" for autodidacts, outlined for/by
[me](/drawings) (its being formally _written out_ like this is by a lunch with
another artist where there was discussion about the value of externalizing
stages of learning).

## Curriculum aka Tour of Resources

This "curriculum" is less curriculum and more of a hypothetical tour of content
I'm excited to keep making progress on. Where dates are mentioned, it means I'm
strangely trying to track an effort to take the curriculum literally.

### Semester: Anatomy w/Tom Fox

Syllabus driven by TOC of: ["Drawing Form and Pose"](/learning#resources) by Tom
Fox, §2 "The Body" (pages 68-260).

**Each week coupled** with:

* [Bammes' "Complete Guide to Anatomy for Artists & Illustrators"](/learning#resources)
* [Vilppu's "Drawing Manual"](/learning#resources)

**Supplemental**: [Bridgman](/learning#resources)

#### Weekly Notes & Living-Syllabus

##### Week 1: 202406 T.Fox's "head"

* **T.Fox's content covered:**
  * From: "starting the head" to "skull width"
  * Pages 72-84
* 20240622 study-notes <!--links redacted-->
* 20240623 " "      " "

##### Week 2: 202406 T.Fox's "head" Cont'd to Occipital

* **T.Fox's content covered:**
  * From: "Occipital" to "silhouette"
  * Pages 85-92
* 20240629 study-notes <!--links redacted-->
* 20240705 " "      " "

##### Week 3: 202407 T.Fox's "head", Blitz'd to the end\!

* **T.Fox's content covered:**
  * From: "face mask" to end of "head" section
  * Pages 93-121
  * **addendum**: off-curriculum "*crash-course on hands*" (as prep for Said's
    "Head & Hands")
    * 1hr co-read through of pages 204-217
* 20240706 study-notes <!--links redacted-->
* 20240706 " "      " "

##### Week 4: 202407: T.Fox Head & Said's Hands

* TFox's content covered, to recap next week:
  * \[2nd-cycle through week 3 content\] pages 94-103
    * 94-97 together
    * 97-103 separate during week
* 20240715 study-notes <!--links redacted-->
* 20240715 study-notes

##### Week 5: 202406: T.Fox Head & Bouftass' Humero-Ulnar Joint

* T.Fox's content covered:
  * Pg 97,98
* 20240720 notes: study session
  * Humerus and ulna look-ahead with TFox and R.B.Hale on Ulna
  * Head's frontal mask with T.Fox

### Semester: Bouftass at P&C & Vilppu

#### Week 6: 202406: Vilppu's Figure Workshop

#### Week 7-11: 2024-07: Bouftass' "Hand & Bit of Head"

#### Week 12: 202409: Vilppu in Vienna

### Semester: Bouftass's Anatomy at P&C

#### Weeks 13-17: 2024-10: Bouftass' "Lower Leg & Foot"

* Photos blackboard <!--gphotos link redacted-->
* Handouts/scans  <!--gdrive link redacted-->

#### Weeks 18-22: 2024-11: Bouftass' "Upper Arm & Shoulder"

* Photos blackboard <!--gphotos link redacted-->
* Handouts/scans  <!--gdrive link redacted-->

### Semester(½-length): Hales' Masters' Figures

**Syllabus driven by TOC of:** ["*Drawing Lessons from the Great Masters*" by Robert Beverly Hale](/learning#resources)

**Each week coupled** with:

* [Bammes](/learning#resources)
* [Vilppu's manual](/learning#resources)

### Semester: color & painting w/Gurney

**Syllabus driven by TOC of:** new Gurney's gouache book (Otherwise, "Color & Light")

**Each week coupled** with:

* ["Electric State"](/learning#resources)
* [Zbukvic's "Mastering Atmosphere"](/learning#resources)

### Semester: figures freed of reference

**Syllabus driven by TOC of**: one of three memory books Jon has

**Each week coupled** with:

* ["Skillful Huntsman"](/learning#resources) by Robertson
* [Hampton's "Figure Drawing"](/learning#resources)
* Tom Bankroft's two books
* ~~Book on rhythms? (A la O'Reilly rhythms?)~~
* [Vilppu's manual](/learning#resources)
* [Bridgman's Anatomy](/learning#resources)

**Supplemental**: [Bammes](/learning#resources)

### Semester: Animals from Imagination

**Syllabus driven by TOC of:** "Atlas of Animal Anatomy"

### Semester: Story, Composition

**Syllabus driven by TOC of:** "Understanding Comics"??

**Each week coupled** with:

* Vandruff's class
* AIC master study

## Appendix

* [Google Drive folder](https://drive.google.com/drive/folders/1VcblBnjnGlI8BG__1KuXeUadbxGv-1Yp) with further scans, notes, etc.

### General Format for All Semesters

1. Be producing prompts and trying them.
2. Be doing one of previous weeks' prompts at random selection
3. Collect all prompts produced, somewhere easily found (eg: this doc, journal,
   anywhere regular).
4. When collaborating: meet-days' format:
   * First half, **recap**: share notes and new aha-moments from week's study
   * Second half, **forward**:
     * 5-10min: skim chapter quietly
     * remainder of the time: read aloud, taking notes, discussing
       * Tip: keep an eye on the time, maybe set a ½ way timer

### Books & Other Sources

See [this page's "other resources" section](/learning).

### Reference Tools

* Digital anatomy apps and sites
  * "Skully" android app, by Proko: https://proko.com/skellyapp
  * https://posemaniacs.com
  * ["Essential Anatomy" android app by "3D Medical OU"][Anatomy3DViewer3]
    (`com.AnatomyLearning.Anatomy3DViewer3`)

[Anatomy3DViewer3]: https://play.google.com/store/apps/details?id=com.AnatomyLearning.Anatomy3DViewer3
