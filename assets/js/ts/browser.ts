import {assert} from './assert.ts';
import {
  Opt,
  optIsPresent,
  optEmpty,
} from './optional.ts';

export type ElemAttrs = {[attr: string]: string};

export class Dom {
  constructor(private windowDoc: Document) {}

  get doc(): Document {
    return this.windowDoc;
  }

  ready(): Promise<void> {
    if (this.doc.readyState === 'complete') {
      return Promise.resolve();
    }
    return new Promise((resolve, reject) => {
      this.doc.addEventListener('readystatechange', () => {
        if (this.doc.readyState === 'complete') {
          resolve();
        }
      });
    });
  }

  query(q: string): Opt<Element> {
    const el = this.doc.querySelector(q);
    return optIsPresent(el) ? el : optEmpty();
  }

  queryOne(q: string): Element {
    return assert(
      this.query(q),
      `DOM query failed; expected to find: "${q}"`);
  }

  appendNewEl(
    parent: Element,
    nodeName: string,
    attrs: ElemAttrs = {},
  ): Element {
    const el = this.buildEl(nodeName, attrs);
    parent.appendChild(el);
    return el;
  }

  buildEl(
    nodeName: string,
    attrs: ElemAttrs = {},
  ): Element {
    const el: Element = assert(
      this.doc.createElement(nodeName),
      `bug? somehow DOM couldn't make createElement("${nodeName}")`);
    Dom.setAttrs(el, attrs);
    return el;
  }

  static setAttrs(e: Element, attrs: ElemAttrs = {}) {
    for (let attr in attrs) {
      e.setAttribute(attr, attrs[attr]);
    }
    return e;
  }
}

export function fetchAsJson<T>(jsonPath: string): Promise<T> {
  return fetch(jsonPath).then(resp => resp.json());
}
