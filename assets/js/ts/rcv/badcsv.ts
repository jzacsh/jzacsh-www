import { assert, countDupes } from './assert.ts';

export type CsvColumnarRow  = { [keyof: string]: string };

// TODO: upstream CsvData back into bin/lib/voterank
interface CsvData {
  headerRowColumns: Array<string>;
  rows: Array<CsvColumnarRow>;
}

export interface ParseCsvOpts {
  // Allows for some cells to be defined without surrounding double quotes.
  lazyQuote: boolean,
  quote: string,
  delim: string,
};

const DEFAULT_PARSE_OPTS: ParseCsvOpts = {
  delim: ',',
  quote: '"',
};

class BadCsvParser {
  constructor(
    private opts: ParseCsvOpts = DEFAULT_PARSE_OPTS,
  ) {
    BadCsvParser.assertValidOpts({
      ...DEFAULT_PARSE_OPTS,
      ...opts,
    });
  }

  private get optLazyQuote(): boolean {
    return this.opts.lazyQuote;
  }

  private get optQuote(): string {
    return this.opts.quote;
  }
  private get optDelim(): string {
    return this.opts.delim;
  }

  public parse(csvContent: string): CsvData {
    const lines = assert(
        (csvContent || '').trim(),
        'require non-empty input'
    ).split('\n');
    assert(
        lines.length >= 2,
        `csv: need >= 2 lines (header and >=1 rows of contente), but got ${
          lines.length
        } lines of CSV`);

    const header = assert(
        lines.shift().trim(),
        'asserted lines count, but shift() gave no real content for header');

    const headerRowColumns = this.mustGetCells(header, 0 /*debugRowIndex*/);
    assert(
        headerRowColumns.length >= 3,
        `voting: header must have >=3 cols, but found ${headerRowColumns.length}`);
    assert(
        !countDupes(headerRowColumns),
        () => `csv: key-mapping for rows will be impossible due to ${
            countDupes(headerRowColumns)
            } duplicate values in the header row`);

    const rows = lines.map((ln, i) =>
        this.mustGetCells(ln, i /*debugRowIndex*/, headerRowColumns.length));

    return {
      headerRowColumns,
      rows: rows.map((row, debugRowIdx) => row.reduce((map, cellVal, colIdx) => {
        const key = BadCsvParser.mustGetLabel(colIdx, headerRowColumns, debugRowIdx);
        map[key] = cellVal;
        return map;
      }, {} /*initialValue*/)),
    };
  }

  private mustGetCells(
      row: string,
      debugRowIndex: number,
      expectingCols = 0,
  ): Array<string> {
    const cells = row.split(this.optDelim).map(cell => cell.trim());
    assert(cells.length >= 1, `csv: a row must have >= 1 cols, but found ${cells.length}`);
    if (expectingCols) {
      assert(
        cells.length == expectingCols,
        `csv: application expecting >= ${
          expectingCols} cols for all rows, but found ${
          cells.length}`);
    }

    return cells.map((cell, i) => this.mustGetCell(cell, i, debugRowIndex));
  }

  private simpleStripQuotes(content: string): string {
    return content.
        slice(this.optQuote.length).
        slice(0, -1 * this.optQuote.length);
  };

  private maybeExtractFromQuotes(cell: string): string {
    const trimmed = cell.trim();
    const hasQuotes = Boolean(
        trimmed.length &&
        trimmed.startsWith(this.optQuote) &&
        trimmed.endsWith(this.optQuote));
    if (!hasQuotes) {
      return cell;
    }

    return simpleStripQuotes(trimmed);
  }

  private mustGetCell(
      cell: string,
      debugColIndex: number,
      debugRowIndex: number,
  ): string {
    function cellAssertMsg(shouldMsg: string) {
      return `csv: cell at col #${
          debugColIndex + 1} on row ${
          debugRowIndex + 1}: should ${
          shouldMsg} "${
          cell}"`;
    }

    if (!this.optLazyQuote) {
      const minCellLength = 2 * this.optQuote.length + 1;
      assert(cell.length >= minCellLength, () => cellAssertMsg(
          `have >= ${minCellLength} chars (2 ${
            this.optQuote.length
          }-length quotes and some inner value), but is ${
            cell.length
          } chars long`));
      assert(
          cell[0] === this.optQuote,
          () => cellAssertMsg(`have an opening quote`));
      assert(
          cell[cell.length - 1] === this.optQuote,
          () => cellAssertMsg(`have a closing quote`));

      // Drop the first and last char (the quotes).
      return assert(
          this.simpleStripQuotes(cell),
          () => cellAssertMsg(`empty content quote (optLazyQuote=${this.optLazyQuote})`));

    }

    assert(
        (cell || '').trim().length >= 1,
        () => cellAssertMsg('be non-empty/all-whitespace'));

    return assert(
        this.maybeExtractFromQuotes(cell),
        () => cellAssertMsg(`empty content quote (optLazyQuote=${this.optLazyQuote})`));
  }

  private static mustGetLabel(
      colIdx: number,
      colLabels: Array<string>,
      debugRowIdx: number,
  ): string {
    assert(
        colIdx >= 0 && colIdx < colLabels.length,
        `bug? pre-validated row ${
            debugRowIdx + 1
        } somehow has a non-sensical col idx: ${
            colIdx
        } expected int in [0, ${
            colLabels.length -1
        }]`);
    return colLabels[colIdx];
  }

  private static assertValidOpts(opts: ParseCsvOpts) {
    function assertNonEmptyStr(val: any, context: string) {
      assert(
          typeof val === 'string' && (val || '').length > 0,

          `require non-empty string for ${
            context
          }, but got: "${
            val
          }" (${
            (val || '').length
          })`);
    }
    assertNonEmptyStr(opts.delim, 'delim');
    assertNonEmptyStr(opts.quote, 'quote char');
    assert(
        opts.delim !== opts.quote &&
            !opts.delim.includes(opts.quote) &&
            !opts.quote.includes(opts.delim),
        `delim ("${
          opts.delim
        }") and quote ("${
          opts.quote
        }") must be mutually exclusive and one cannot be a substr of the other`);
  }
}

export function parse(
    csvContent: string,
    opts: ParseCsvOpts = DEFAULT_PARSE_OPTS,
): CsvData {
  return new BadCsvParser(opts).parse(csvContent);
}
