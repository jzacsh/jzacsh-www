/** necessary for newer Map objects when you're trying to debug (eg: via JSON.stringify() to console. */
export function toPlainOldObject(o: Map<unknown, unknown>|Object|null|Array<unknown>) {
  function recurseOnEntries(a: Array<unknown>) {
    return Object.fromEntries(
      a.map(([k, v]) => [k, toPlainOldObject(v)])
    );
  }
  
  if (o instanceof Map) {
    return recurseOnEntries([...o]);
  }
  if (Array.isArray(o)) {
    return o.map(toPlainOldObject);
  }
  if (typeof o === "object" && o !== null) {
    return recurseOnEntries(Object.entries(o));
  }
  return o;
}
