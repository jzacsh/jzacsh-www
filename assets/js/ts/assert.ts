// See https://www.typescriptlang.org/docs/handbook/utility-types.html

/** Scalars that JavaScript typically treats as false. */
export type AssertFalsey = false | undefined | null | 0 | "";

/** T we can safely assert is non-falsey, the opposite of {@link Unasserted<T>}. */
export type Asserted<T> = Exclude<T, AssertFalsey>;

// TODO maybe rethink this; useful for void assertions.
type AssertedAny = Asserted<any>;

/** Content that's yet to be asserted, thus opposite to Asserted<T>. */
export type Unasserted<T> = T | AssertFalsey;

export type AssertDebugger = string|Function;

// see https://www.typescriptlang.org/docs/handbook/2/narrowing.html#using-type-predicates
function assertPasses<T>(val: Unasserted<T>): val is Asserted<T> {
  return !!val;

  /*  TODO why does tsc seem to need this instead of letting me cast to bool?
  val !== false &&
  val !== undefined &&
  val !== null &&
  val !== 0 &&
  val !== '';
  */
}

export function assert<T>(
  content: Unasserted<T>,
  errorMsgOrFn: AssertDebugger,
): Asserted<T> {
  if (assertPasses(content)) return content;

  const errorMsg: string = typeof errorMsgOrFn === 'function' ?
    errorMsgOrFn() :
    errorMsgOrFn;
  throw new AssertionError(errorMsg);
}
export class AssertionError extends Error {
	constructor(message: string, options?: any) {
		super(`assertion failed: ${message}`, options);
	}
}
