import {assert} from './assert.ts';

export function strTrim(str: string, needle: string): string {
  const oneOrMoreLeading = new RegExp(`^${needle}+`);
  const oneOrMoreTrailing = new RegExp(`${needle}+$`);
  return str.
      replace(oneOrMoreLeading, '').
      replace(oneOrMoreTrailing, '');
}

export function strTrimLeft(str: string, toRemove: string): string {
  assert(
      str.length > toRemove.length,
      `string to remove ("${toRemove}") should be shorter than content ("${str}")`);
  const leftTrim: string = str.substring(0, toRemove.length /*indexEndExclusive*/);
  if (leftTrim !== toRemove) return str;
  return str.substring(toRemove.length);
}

export function strLastChar(str: string): string {
  return str[str.length - 1];
}
