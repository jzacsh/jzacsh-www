import { Dom } from './browser.ts'
import { assert } from './assert.ts'

// TODO: figure out how to notify myself this dep is out of date; maybe just
// track all external deps via Makefile and check their hash against upstream
// at build time?
//  ./voterank.ts
//  ./debug.ts
//... or maybe just properly publish to jsr. Seems annoying, but maybe less
//annoying that anything else.
import { RunoffResults, WeightedScoreResults, UserBallot, BallotCheckState, BallotCheck, VoteMachine } from './rcv/voterank.ts';
import { toPlainOldObject } from './rcv/debug.ts';
import { assertPositiveInt, countDupes } from './rcv/assert.ts';

// import { parse, ParseResult } from 'jsr:@std/csv';
// TODO: figure out how to get jsr imports (above) working with hugo's js.Build
// (see layouts/shortcodes/ts.html). In the meantime we use this super brittle
// hand-written not-really-a-parser I'd be happy to just delete:
import {
  ParseCsvOpts,
  parse,
  CsvColumnarRow,
} from './rcv/badcsv.ts';

import { CsvData } from './rcv/csv.ts';

// TODO: is there a modern version of this? or this is fine?
function deepClone(o: unknown): unknown {
  return JSON.parse(JSON.stringify(o));
}

interface PageOpts {
  // Integer indicating how many of the columns (starting from the left/start)
  // to ignore/drop before pass
  dropCols: number;
  csvOpts: ParseCsvOpts;
};

interface ProcessResult {
  outputString: string;
  outputStruct?: {
    parsedCsv: CsvData;
    voteMachine: VoteMachine;
    resultWeightedAlgo: WeightedScoreResults;
    resultRcvRunoff: RunoffResults
  };
}

function shiftFirstNCols(csvData: CsvData, dropCols: number) {
  for (let i = 0; i < dropCols; i++) {
    const discardedColLabel = csvData.headerRowColumns.shift();
    const rows = csvData.rows.map(row => {
      delete row[discardedColLabel];
      return row;
    });
    csvData.rows = rows;
  }
}

// TODO: move custom handling of PageOpts params (internally in this function)
// back upstream to my dotfiles bin/lib/voterank script.
function processNewInput(
  csvInput?: string,
  pageOpts: PageOpts,
): ProcessResult {
  let output: string[] = [];

  let dropCols: number;
  try {
    dropCols = assertPositiveInt(pageOpts.dropCols, 'dropCols form-input');
  } catch (e) {
    output.push(`ERROR: CSV parser: ${e}\n${e.stack.replaceAll('\n', '\n\t')}`);
    return { outputString: output.join('\n'), };
  }

  let voteResults: CsvData;
  try {
    voteResults = parse(csvInput, pageOpts.csvOpts);
    assert(
        voteResults.rows.length >= 2,
        `voting: need >= 2 ballots rows of CSV for a meaningful competition but got ${
            voteResults.rows.length
        } lines of CSV`);
    assert(
        voteResults.headerRowColumns.length >= 2,
        `voting: need >= 2 columns: at least two competing candidates and zero or more voter rows (eg: timestamp, username, etc), but got these ${
            voteResults.headerRowColumns.length
        } CSV columns ["${
            voteResults.headerRowColumns.join('", "')
        }"]`);
  } catch (e) {
    output.push(`ERROR: CSV parser: ${e}\n${e.stack.replaceAll('\n', '\n\t')}`);
    return { outputString: output.join('\n'), };
  }
  const originalCsvData = deepClone(voteResults);

  // drop the first column; VoteMachine doesn't care about voter's names
  shiftFirstNCols(voteResults, dropCols);

  const candidates = voteResults.headerRowColumns;
  const ballots = voteResults.rows.
      map(row => {
        // Strip rank-choice-voting rank-suffixes ("st" for "1st" or "nd" for
        // "2nd" rank) when folks setup google forms RCV forms.
        for (let key in row) {
          const cleanedRank = row[key].replace(/[^\d]+$/g, '');
          row[key] = cleanedRank;
        }
        return row;
      });
  const voteMachine = new VoteMachine(candidates, ballots);

  voteMachine.tallyVotes().
    map((check, i) => {
      return {
        check,
        idx: i,
      };
    }).
    filter(c => c.check.state !== BallotCheckState.Ok).
    forEach(c => {
      output.push(`SKIPPING voter #${
        c.idx} has malformed ballot: ${
        BallotCheckState[c.check.state]}: csv line ${
        c.idx + 2 /*0-index + offset from header row*/}: "${c.check.subject}"`);
    });
  const resultWeightedAlgo = voteMachine.findWinByWeight();
  const resultRcvRunoff = voteMachine.findWinnerRunoff();
  output.push('"""');
  output.push(JSON.stringify(toPlainOldObject({
    voteMachine,
    resultWeightedAlgo,
    resultRcvRunoff,
  })));
  output.push('"""');
  output.push('the above block is valid JSON you can paste into a prettifier');
  if (voteMachine.badBallots.length >= voteMachine.votes.length) {
    output.push("BAD INPUT! your whole CSV is malformed");
  }

  return {
    outputString: output.join('\n'),
    outputStruct: {
      parsedCsv: originalCsvData,
      voteMachine,
      resultWeightedAlgo,
      resultRcvRunoff,
    },
  };
}

// Annoying top-level await wrapping
(async function bootStrapPageGlue() {

const QuerySelector = {
  SCRIPT_OUT: '#rcv-results #scriptout',
  SCRIPT_INPUT: '#rcv-csvpaste textarea',

  CSVOPT_DROPCOLS: '#rcv-formopts .csvopt-dropCols input',
  CSVOPT_DELIM: '#rcv-formopts .csvopt-tsvdelim input',
  CSVOPT_LAZYQUOTE: '#rcv-formopts .csvopt-lazyquote input',
  CSVOPT_QUOTE: '#rcv-formopts .csvopt-quote input',
};

class PageForm {
  public inputEl;

  private csvOptEls = {};

  constructor(private dom: Dom) {
    this.inputEl = this.dom.queryOne(QuerySelector.SCRIPT_INPUT);
    this.csvOptEls = {
      dropCols: dom.queryOne(QuerySelector.CSVOPT_DROPCOLS),
      tsvdelim: dom.queryOne(QuerySelector.CSVOPT_DELIM),
      lazyQuote: dom.queryOne(QuerySelector.CSVOPT_LAZYQUOTE),
      quote: dom.queryOne(QuerySelector.CSVOPT_QUOTE),
    };
  }

  public userCsv(): string {
    return this.inputEl.value;
  }

  public opts(): PageOpts {
    return {
      dropCols: this.csvOptEls.dropCols.value,
      tsvdelim: this.csvOptEls.tsvdelim.checked,
      csvOpts: {
        delim: this.csvOptEls.tsvdelim.checked ? '\t' : ',',
        lazyQuote: this.csvOptEls.lazyQuote.checked,
        quote: this.csvOptEls.quote.value,
      },
    };
  }

  public bindInputEvents(handleFn: (void)) {
    const targetInputEls = {
        inputEl: this.inputEl,
        ...this.csvOptEls,
    };
    for (let key in targetInputEls) {
      targetInputEls[key].addEventListener('input', handleFn);
    }
  }
};

const dom = new Dom(window.document);
await dom.ready();
const pageForm = new PageForm(dom);
const outputEl = dom.queryOne(QuerySelector.SCRIPT_OUT);
function handleNewInputElContent() {
  const result = processNewInput(pageForm.userCsv(), pageForm.opts());
  outputEl.value = result.outputString;
  outputEl.value += '\n\n\n';
  if (result.outputStruct) {
    window.rcv = window.rcv || {};
    window.rcv.result = result.outputStruct;
    outputEl.value += `runoff winner is: "${window.rcv.result.resultRcvRunoff.winner}"`;
    outputEl.value += '\nsee `window.rcv.result` in devtools!';
  } else {
    outputEl.value += 'fix error above? if no error above, then this is a bug';
  }
}

handleNewInputElContent(); // handle at least once
pageForm.bindInputEvents(() => handleNewInputElContent());

})();
